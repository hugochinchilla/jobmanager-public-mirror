FROM php:7.2-cli

RUN apt-get update && apt-get install -y git

RUN pecl install xdebug-2.6.0 \
    && docker-php-ext-enable xdebug
