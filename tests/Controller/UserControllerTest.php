<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\JobManager\Application\Actions\RegisterUser\RegisterUser;
use App\JobManager\Infrastructure\Persistence\InMemory\InMemoryUserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends WebTestCase
{
    /** @test */
    public function should_register_user()
    {
        $repo = new InMemoryUserRepository();
        RegisterUser::setDefaultUserRepository($repo);

        $client = static::createClient();
        $client->request('POST', '/signup', [
            'first_name' => 'Ben',
            'last_name' => 'Kenobi',
            'email' => 'ben@resistance.org',
            'password' => 'maytheforce',
        ]);

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals(1, count($repo->all()));
    }

    public function tearDown()
    {
        parent::tearDown();
        RegisterUser::setDefaultUserRepository(null);
    }
}
