<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Application\Actions;

use App\JobManager\Application\Actions\RegisterUser\RegisterUser;
use App\JobManager\Application\Actions\RegisterUser\RegisterUserRequest;
use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\Password;
use App\JobManager\Domain\Entities\PersonalName;
use App\JobManager\Infrastructure\Persistence\InMemory\InMemoryUserRepository;
use App\Tests\JobManager\Builder\UserBuilder;
use PHPUnit\Framework\TestCase;

class RegisterUserTest extends TestCase
{
    private $repo;
    private $builder;
    private $srv;

    public function setUp(): void
    {
        $this->repo = new InMemoryUserRepository();
        $this->builder = new UserBuilder($this->repo);
        $this->srv = new RegisterUser($this->repo);
    }

    /** @test */
    public function should_save_the_user()
    {
        $request = new RegisterUserRequest(
        new PersonalName('John Smith'),
        new EmailAddress('john@example.org'),
        new Password('12345678'));

        $this->srv->execute($request);

        $users = $this->repo->all();
        $this->assertNotEmpty($users);
    }

    /** @test */
    public function should_fail_for_repeated_emails()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('john@example.org already in use');

        $email = new EmailAddress('john@example.org');
        $this->builder
            ->withEmail($email)
            ->build();
        $request = new RegisterUserRequest(
        new PersonalName('John Smith'),
        $email,
        new Password('12345678'));

        $this->srv->execute($request);
    }
}
