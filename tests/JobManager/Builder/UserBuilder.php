<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Builder;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\Password;
use App\JobManager\Domain\Entities\PersonalName;
use App\JobManager\Domain\Entities\UUID;
use App\JobManager\Domain\Model\User\User;
use App\JobManager\Domain\Model\User\UserRepository;
use App\JobManager\Infrastructure\Persistence\InMemory\InMemoryUserRepository;

class UserBuilder
{
    private $id;
    private $email;
    private $name;
    private $password;
    private $repo;

    public function __construct(UserRepository $repository = null)
    {
        $this->id = new UUID();
        $this->email = new EmailAddress('johndoe@example.org', 'John Doe');
        $this->name = new PersonalName('John', 'Doe');
        $this->password = new Password('12345678');

        $this->repository = $repository ?? new InMemoryUserRepository();
    }

    public function withId(UUID $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function withName(PersonalName $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withEmail(EmailAddress $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function withPassword(Password $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function build()
    {
        $user = new User($this->id, $this->name, $this->email, $this->password);

        $this->repository->save($user);

        return $user;
    }
}
