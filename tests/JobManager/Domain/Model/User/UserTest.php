<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Domain\Model\User;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\HashedPassword;
use App\JobManager\Domain\Entities\Password;
use App\JobManager\Domain\Entities\PersonalName;
use App\JobManager\Domain\Entities\UUID;
use App\JobManager\Domain\Model\User\User;
use App\JobManager\Infrastructure\TestCase;

class UserTest extends TestCase
{
    public function setUp(): void
    {
        $this->id = new UUID();

        $this->user = new User(
            $this->id,
            new PersonalName('John'),
            new EmailAddress('john@example.org'),
            new Password('12345678'));
    }

    /** @test */
    public function should_preserve_id()
    {
        $this->assertEquals($this->id, $this->user->id());
    }

    /** @test */
    public function should_preserve_name()
    {
        $this->assertEquals(new PersonalName('John'), $this->user->name());
    }

    /** @test */
    public function should_preserve_email()
    {
        $this->assertEquals(new EmailAddress('john@example.org'), $this->user->email());
    }

    /** @test */
    public function should_prevent_from_reading_plaintext_password()
    {
        $this->assertInstanceOf(HashedPassword::class, $this->user->password());
    }

    /** @test */
    public function should_preserve_hashed_passwords()
    {
        $hash = '$2y$10$MTQWvXRHce.JF0mqzGRso.Ck7NbGDk5rEn9r7Nf6b8wqz2env7BBS';
        $user = new User(
            new UUID(),
            new PersonalName('John'),
            new EmailAddress('john@example.org'),
            new HashedPassword($hash));

        $this->assertInstanceOf(HashedPassword::class, $this->user->password());
    }
}
