<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Domain\Model\User;

use App\JobManager\Domain\Entities\HashedPassword;
use App\JobManager\Domain\Entities\Password;
use App\JobManager\Domain\Model\User\PasswordHasher;
use PHPUnit\Framework\TestCase;

class PasswordHasherTest extends TestCase
{
    /** @test */
    public function should_return_a_hashed_password()
    {
        $hasher = new PasswordHasher();

        $hash = $hasher->hash(new Password('12345678'));

        $this->assertInstanceOf(HashedPassword::class, $hash);
    }

    /** @test */
    public function should_be_able_to_verify_hash_against_password()
    {
        $hasher = new PasswordHasher();

        $hash = $hasher->hash(new Password('12345678'));

        $this->assertTrue($hash->verify('12345678'));
    }
}
