<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Domain\Entities;

use App\JobManager\Domain\Entities\InvalidValueException;
use App\JobManager\Domain\Entities\UUID;
use PHPUnit\Framework\TestCase;

class UUIDTest extends TestCase
{
    /** @test */
    public function should_generate_new_id_if_not_initialized()
    {
        $id = new UUID();

        $this->assertNotEmpty($id->value());
    }

    /** @test */
    public function should_reject_initialization_with_non_valid_value()
    {
        $this->expectException(InvalidValueException::class);

        $id = new UUID('');
    }

    /** @test */
    public function should_preserve_value_given_at_contruction_time()
    {
        $an_uid = '35e9eeda-a7dc-4c57-88e9-2949d8029e97';

        $id = new UUID($an_uid);

        $this->assertEquals($an_uid, $id->value());
    }

    /** @test */
    public function should_cast_to_string()
    {
        $an_uid = '35e9eeda-a7dc-4c57-88e9-2949d8029e97';

        $id = new UUID($an_uid);

        $this->assertEquals($an_uid, (string) $id);
    }
}
