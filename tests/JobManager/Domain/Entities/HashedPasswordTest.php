<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Domain\Entities;

use App\JobManager\Domain\Entities\InvalidValueException;
use App\JobManager\Domain\Entities\HashedPassword;
use PHPUnit\Framework\TestCase;

class HashedPasswordTest extends TestCase
{
    /** @test */
    public function should_preserve_its_value()
    {
        $hash = '$2y$10$MTQWvXRHce.JF0mqzGRso.Ck7NbGDk5rEn9r7Nf6b8wqz2env7BBS';

        $password = new HashedPassword($hash);

        $this->assertEquals($hash, $password->value());
    }

    /** @test */
    public function should_cast_to_string()
    {
        $hash = '$2y$10$MTQWvXRHce.JF0mqzGRso.Ck7NbGDk5rEn9r7Nf6b8wqz2env7BBS';

        $password = new HashedPassword($hash);

        $this->assertEquals($hash, (string) $password);
    }

    /** @test */
    public function should_validate_hash_is_valid()
    {
        $this->expectException(InvalidValueException::class);

        $password = new HashedPassword('not a valid hash');
    }
}
