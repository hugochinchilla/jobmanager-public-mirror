<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Domain\Entities;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\InvalidValueException;
use PHPUnit\Framework\TestCase;

class EmailAddressTest extends TestCase
{
    /** @test */
    public function should_be_valid_email()
    {
        $this->expectException(InvalidValueException::class);

        new EmailAddress('foo');
    }

    /** @test */
    public function should_retain_vaild_email()
    {
        $address = new EmailAddress('admin@example.org');

        $this->assertEquals('admin@example.org', $address->email());
    }

    /** @test */
    public function should_retain_name_if_provided()
    {
        $address = new EmailAddress('admin@example.org', 'Admin');

        $this->assertEquals('Admin', $address->name());
    }

    /** @test */
    public function should_reject_empty_names()
    {
        $this->expectException(InvalidValueException::class);

        $address = new EmailAddress('admin@example.org', '');
    }

    /** @test */
    public function name_is_null_when_not_provided()
    {
        $address = new EmailAddress('admin@example.org');

        $this->assertEquals(null, $address->name());
    }

    /** @test */
    public function should_cast_to_string()
    {
        $address = new EmailAddress('admin@example.org', 'John Doe');

        $this->assertEquals('"John Doe" <admin@example.org>', (string) $address);
    }

    /** @test */
    public function should_escape_quotes_when_casted_to_string()
    {
        $address = new EmailAddress('admin@example.org', 'John "Doe"');

        $this->assertEquals('"John \"Doe\"" <admin@example.org>', (string) $address);
    }
}
