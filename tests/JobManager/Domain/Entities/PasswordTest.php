<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Domain\Entities;

use App\JobManager\Domain\Entities\InvalidValueException;
use App\JobManager\Domain\Entities\Password;
use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{
    /** @test */
    public function should_fail_for_passwords_under_8_chars()
    {
        $this->expectException(InvalidValueException::class);

        new Password('1234567');
    }

    /** @test */
    public function should_cast_to_string()
    {
        $password = new Password('12345678');

        $this->assertEquals('12345678', (string) $password);
    }
}
