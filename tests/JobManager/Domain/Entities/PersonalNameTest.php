<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Domain\Entities;

use App\JobManager\Domain\Entities\InvalidValueException;
use App\JobManager\Domain\Entities\PersonalName;
use PHPUnit\Framework\TestCase;

class PersonalNameTest extends TestCase
{
    /** @test */
    public function should_retain_first_name()
    {
        $name = new PersonalName('John', 'Doe');

        $this->assertEquals('John', $name->first());
    }

    /** @test */
    public function should_retain_last_name()
    {
        $name = new PersonalName('John', 'Doe');

        $this->assertEquals('Doe', $name->last());
    }

    /** @test */
    public function first_name_is_required()
    {
        $this->expectException(InvalidValueException::class);

        $name = new PersonalName('', 'Doe');
    }

    /** @test */
    public function last_name_is_not_required()
    {
        $name = new PersonalName('John');

        $this->assertEquals(null, $name->last());
    }

    /** @test */
    public function should_convert_empty_last_name_to_null()
    {
        $name = new PersonalName('John', '');

        $this->assertEquals(null, $name->last());
    }

    /** @test */
    public function should_format_name_when_casted_to_string()
    {
        $name = new PersonalName('John', 'Doe');

        $this->assertEquals('John Doe', (string) $name);
    }

    /** @test */
    public function should_not_add_extra_spaces_when_casting_name_only()
    {
        $name = new PersonalName('John');

        $this->assertEquals('John', (string) $name);
    }
}
