<?php

declare(strict_types=1);

namespace App\Tests\JobManager\Infrastructure\Persitence\User;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\PersonalName;
use App\JobManager\Domain\Entities\UUID;
use App\JobManager\Infrastructure\Persistence\InMemory\InMemoryUserRepository;
use App\JobManager\Infrastructure\TestCase;
use App\Tests\JobManager\Builder\UserBuilder;

class InMemoryUserRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        $this->repo = new InMemoryUserRepository();
        $this->builder = new UserBuilder($this->repo);
    }

    /** @test */
    public function should_be_able_to_retrieve_saved_users()
    {
        $id = new UUID('f74e98e4-1148-4239-9433-160bc21713ec');
        $user = $this->builder
            ->withId($id)
            ->build();

        $result = $this->repo->findByUuid($id);

        $this->assertEquals($user, $result);
    }

    /** @test */
    public function should_be_able_to_retrieve_all_users()
    {
        $user = $this->builder->build();

        $result = $this->repo->all();

        $this->assertEquals([$user], $result);
    }

    /** @test */
    public function should_be_able_to_update_existent_users()
    {
        $id = new UUID('f74e98e4-1148-4239-9433-160bc21713ec');
        $this->builder
            ->withId($id)
            ->build();

        $the_name = new PersonalName('John', 'Williams');
        $this->builder
            ->withId($id)
            ->withName($the_name)
            ->build();

        $result = $this->repo->findByUuid($id);
        $this->assertEquals($the_name, $result->name());
    }

    /** @test */
    public function should_be_able_to_delete_saved_users()
    {
        $id = new UUID('f74e98e4-1148-4239-9433-160bc21713ec');
        $user = $this->builder
            ->withId($id)
            ->build();

        $this->repo->delete($id);

        $result = $this->repo->findByUuid($id);
        $this->assertNull($result);
    }

    /** @test */
    public function should_be_able_to_search_by_email()
    {
        $user = $this->builder
            ->withEmail(new EmailAddress('foo@example.org'))
            ->build();

        $result = $this->repo->findByEmail(new EmailAddress('foo@example.org'));

        $this->assertNotNull($result);
    }
}
