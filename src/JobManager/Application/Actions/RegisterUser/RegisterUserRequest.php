<?php

declare(strict_types=1);

namespace App\JobManager\Application\Actions\RegisterUser;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\Password;
use App\JobManager\Domain\Entities\PersonalName;

class RegisterUserRequest
{
    private $name;
    private $email;
    private $password;

    public function __construct(
        PersonalName $name,
        EmailAddress $email,
        Password $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    public function name()
    {
        return $this->name;
    }

    public function email()
    {
        return $this->email;
    }

    public function password()
    {
        return $this->password;
    }

    public static function fromRequest($request)
    {
        return new self(
            new PersonalName($request->get('first_name', ''), $request->get('last_name', '')),
            new EmailAddress($request->get('email', '')),
            new Password($request->get('password', '')));
    }
}
