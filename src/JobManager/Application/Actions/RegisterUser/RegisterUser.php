<?php

declare(strict_types=1);

namespace App\JobManager\Application\Actions\RegisterUser;

use App\JobManager\Domain\Entities\UUID;
use App\JobManager\Domain\Model\User\User;
use App\JobManager\Domain\Model\User\UserRepository;

class RegisterUser
{
    private $repo;
    private static $default_repo;

    public function __construct(UserRepository $repo)
    {
        $this->repo = self::$default_repo ?? $repo;
    }

    public function execute(RegisterUserRequest $request): RegisterUserResponse
    {
        $id = new UUID();

        if (null !== $this->repo->findByEmail($request->email())) {
            $email = $request->email()->email();
            throw new \Exception("{$email} already in use");
        }

        $user = new User($id, $request->name(), $request->email(), $request->password());

        $this->repo->save($user);

        return new RegisterUserResponse($user);
    }

    public function getRequestClass()
    {
        return RegisterUserRequest::class;
    }

    public static function setDefaultUserRepository(UserRepository $repo = null)
    {
        self::$default_repo = $repo;
    }
}
