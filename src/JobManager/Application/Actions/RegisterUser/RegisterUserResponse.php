<?php

declare(strict_types=1);

namespace App\JobManager\Application\Actions\RegisterUser;

use App\JobManager\Domain\Model\User\User;

class RegisterUserResponse
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
