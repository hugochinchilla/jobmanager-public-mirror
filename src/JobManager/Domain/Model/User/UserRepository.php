<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Model\User;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\UUID;

interface UserRepository
{
    public function save(User $user): void;

    public function delete(UUID $uuid): void;

    public function all(): array;

    public function findByUuid(UUID $uuid): ?User;

    public function findByEmail(EmailAddress $email): ?User;
}
