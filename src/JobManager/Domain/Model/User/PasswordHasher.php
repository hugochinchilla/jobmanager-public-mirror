<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Model\User;

use App\JobManager\Domain\Entities\HashedPassword;
use App\JobManager\Domain\Entities\Password;

class PasswordHasher
{
    public function hash(Password $password): HashedPassword
    {
        $hash = password_hash($password->value(), PASSWORD_DEFAULT);

        return new HashedPassword($hash);
    }
}
