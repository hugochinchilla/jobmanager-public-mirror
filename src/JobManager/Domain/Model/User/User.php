<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Model\User;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\Password;
use App\JobManager\Domain\Entities\PasswordInterface;
use App\JobManager\Domain\Entities\PersonalName;
use App\JobManager\Domain\Entities\UUID;

class User
{
    private $id;
    private $name;
    private $email;
    private $password;

    public function __construct(
        UUID $id,
        PersonalName $name,
        EmailAddress $email,
        PasswordInterface $password,
        PasswordHasher $hasher = null)
    {
        if (null === $hasher) {
            $hasher = new PasswordHasher();
        }

        if ($password instanceof Password) {
            $password = $hasher->hash($password);
        }

        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    public function id()
    {
        return $this->id;
    }

    public function name()
    {
        return $this->name;
    }

    public function email()
    {
        return $this->email;
    }

    public function password()
    {
        return $this->password;
    }
}
