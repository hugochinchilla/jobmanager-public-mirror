<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

class EmailAddress
{
    private $name;
    private $email;

    public function __construct(string $email, string $name = null)
    {
        if ($this->isNotValidEmail($email)) {
            throw new InvalidValueException("$email is not a valid email");
        }

        if ($this->nameIsEmpty($name)) {
            throw new InvalidValueException("Name can't be an empty string, try null instead");
        }

        $this->email = $email;
        $this->name = $name;
    }

    public function __toString(): ?string
    {
        $escaped_name = addslashes($this->name);

        return "\"{$escaped_name}\" <{$this->email}>";
    }

    public function name(): ?string
    {
        return $this->name;
    }

    public function email(): string
    {
        return $this->email;
    }

    private function isNotValidEmail(string $email): bool
    {
        return !filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    private function nameIsEmpty(string $name = null): bool
    {
        return '' === $name;
    }
}
