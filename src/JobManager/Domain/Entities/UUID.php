<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

use Ramsey\Uuid\Uuid as UuidLib;

class UUID
{
    private $value;

    public function __construct(string $uuid = null)
    {
        if (null === $uuid) {
            $uuid = UuidLib::uuid4()->toString();
        } elseif ($this->isInvalidUuid($uuid)) {
            throw new InvalidValueException("$uuid is not a valid UUID v4");
        }

        $this->value = $uuid;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function value(): string
    {
        return $this->value;
    }

    private function isInvalidUuid(string $uuid): bool
    {
        return !UuidLib::isValid($uuid);
    }
}
