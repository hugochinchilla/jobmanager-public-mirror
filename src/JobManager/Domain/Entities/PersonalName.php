<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

class PersonalName
{
    private $first_name;
    private $last_name;

    public function __construct(string $first_name, string $last_name = null)
    {
        if (empty($first_name)) {
            throw new InvalidValueException("first name can't be empty");
        }

        if ('' === $last_name) {
            $last_name = null;
        }

        $this->first_name = $first_name;
        $this->last_name = $last_name;
    }

    public function __toString(): string
    {
        return trim("{$this->first_name} {$this->last_name}");
    }

    public function first(): string
    {
        return $this->first_name;
    }

    public function last(): ?string
    {
        return $this->last_name;
    }
}
