<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

class InvalidValueException extends \Exception
{
}
