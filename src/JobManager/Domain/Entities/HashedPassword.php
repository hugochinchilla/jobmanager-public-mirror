<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

class HashedPassword implements PasswordInterface
{
    private $value;

    public function __construct(string $password)
    {
        if ($this->isInvalidHash($password)) {
            throw new InvalidValueException('not a valid password hash');
        }

        $this->value = $password;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function value()
    {
        return $this->value;
    }

    public function verify(string $password): bool
    {
        return password_verify($password, $this->value);
    }

    private function isInvalidHash(string $value)
    {
        $info = password_get_info($value);

        return 'unknown' === $info['algoName'];
    }
}
