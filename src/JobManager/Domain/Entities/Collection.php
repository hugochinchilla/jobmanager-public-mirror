<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

interface Collection
{
    public function add($obj, $key = null);

    public function delete($key);

    public function get($key);
}
