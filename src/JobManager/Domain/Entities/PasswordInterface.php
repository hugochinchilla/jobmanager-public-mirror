<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

interface PasswordInterface
{
    public function value();
}
