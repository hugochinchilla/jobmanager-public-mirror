<?php

declare(strict_types=1);

namespace App\JobManager\Domain\Entities;

class Password implements PasswordInterface
{
    private $value;

    public function __construct(string $password)
    {
        if ($this->isTooShort($password)) {
            throw new InvalidValueException('Password is too short');
        }

        $this->value = $password;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function value()
    {
        return $this->value;
    }

    private function isTooShort(string $value)
    {
        return mb_strlen($value) < 8;
    }
}
