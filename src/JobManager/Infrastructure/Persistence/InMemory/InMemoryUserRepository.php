<?php

declare(strict_types=1);

namespace App\JobManager\Infrastructure\Persistence\InMemory;

use App\JobManager\Domain\Entities\EmailAddress;
use App\JobManager\Domain\Entities\UUID;
use App\JobManager\Domain\Model\User\User;
use App\JobManager\Domain\Model\User\UserRepository;

class InMemoryUserRepository implements UserRepository
{
    private $items = [];

    public function save(User $user): void
    {
        $this->items[$user->id()->value()] = $user;
    }

    public function delete(UUID $uuid): void
    {
        unset($this->items[$uuid->value()]);
    }

    public function all(): array
    {
        return array_values($this->items);
    }

    public function findByUuid(UUID $uuid): ?User
    {
        return $this->items[$uuid->value()] ?? null;
    }

    public function findByEmail(EmailAddress $email): ?User
    {
        foreach ($this->items as $user) {
            if ($user->email()->email() == $email->email()) {
                return $user;
            }
        }

        return null;
    }
}
