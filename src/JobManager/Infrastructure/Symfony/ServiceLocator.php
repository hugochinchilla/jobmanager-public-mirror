<?php

declare(strict_types=1);

namespace App\JobManager\Infrastructure\Symfony;

use Symfony\Component\HttpFoundation\Request;

class ServiceLocator
{
    private $actions;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function executeService(string $identifier, Request $request)
    {
        $srv = $this->container->get($identifier);

        return $srv->execute($request);
    }
}
