<?php

declare(strict_types=1);

namespace App\JobManager\Infrastructure\Symfony;

class PublicService
{
    private $actions;

    public function __construct()
    {
        $this->actions = [];
    }

    public function add($action)
    {
        $this->actions[] = $action;
    }

    public function execute($request)
    {
        $response = [];
        foreach ($this->actions as $action) {
            $reqClass = $action->getRequestClass();
            $req = call_user_func([$reqClass, 'fromRequest'], $request);

            $response[] = $action->execute($req);
        }

        return $response;
    }
}
