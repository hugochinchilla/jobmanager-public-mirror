<?php

declare(strict_types=1);

namespace App\Controller;

use App\JobManager\Infrastructure\Symfony\ServiceLocator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    public function signUp(Request $request, ServiceLocator $locator): JsonResponse
    {
        $locator->executeService('user_signup', $request);

        $response = new JsonResponse(['status' => 'success']);
        $response->setStatusCode(Response::HTTP_CREATED);

        return $response;
    }
}
