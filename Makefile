.PHONY: test

test:
	docker-compose run --rm php vendor/bin/phpunit

coverage:
	docker-compose run --rm php vendor/bin/phpunit --coverage-text

coverage-html:
	docker-compose run --rm php vendor/bin/phpunit --coverage-html coverage

shell:
	docker-compose run --rm php bin/console psysh

